## v0.1.0 (2021-05-17)

### Feat

- **orgchart**: add filter function to organization list and get method
- **orgchart**: add employees and supervisor property when iterate through employees
- **orgchart**: add employees and supervisor property to organization resource when OrgChart class initialization
- **orgchart**: support filter in employee list function, modify get method to use list function and raise error when too many resource found
- **orgchart**: add cache in employee list method
- **orgchart**: add employee resources and its list and get method
- **orgchart**: modify data structure for organizations and add get organization method
- **orgchart**: add Resource, ResourceManager, and add organization manager to deal with organization list method
- **orgchart**: add basic class
