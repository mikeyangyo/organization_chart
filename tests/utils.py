from orgchart.resources import Resource


def resource_test(resource: Resource, **kwargs) -> bool:
    # check all key, value defined in kwargs will be same w/ resource property
    for key, value in kwargs.items():
        if not hasattr(resource, key):
            return False
        if getattr(resource, key, None) != value:
            return False
    return True
