from orgchart import OrgChart
from orgchart.resources import (
    Organization,
    Employee,
    exceptions,
    ResourceManager,
)
from .utils import resource_test

import unittest
from pytest import raises


class OrganizationTest(unittest.TestCase):
    def setUp(self):
        self.org_chart = OrgChart("organizations.json", "employees.json")

    def test_list_organizations(self):
        organizations = self.org_chart.organization.list()
        assert isinstance(organizations, list)
        assert len(organizations) == 5

        # 1st organization properties test
        organization = organizations[0]
        assert resource_test(
            organization,
            **{
                "id": 1,
                "parent_id": 0,
                "organization_code": "president-office",
                "organization_name": "President Office",
                "supervisor_id": "0101001",
                "is_active": True,
            },
        )

        # 2nd organization properties test
        organization = organizations[1]
        assert resource_test(
            organization,
            **{
                "id": 2,
                "parent_id": 1,
                "organization_code": "business-dev",
                "organization_name": "Business Development Dept.",
                "supervisor_id": "0101002",
                "is_active": True,
            },
        )

        # 3rd organization properties test
        organization = organizations[2]
        assert resource_test(
            organization,
            **{
                "id": 4,
                "parent_id": 1,
                "organization_code": "product-dev",
                "organization_name": "Product Development Div.",
                "supervisor_id": "0101003",
                "is_active": True,
            },
        )

        # 4th organization properties test
        organization = organizations[3]
        assert resource_test(
            organization,
            **{
                "id": 3,
                "parent_id": 4,
                "organization_code": "product-design",
                "organization_name": "Product Design Dept.",
                "supervisor_id": "0101003",
                "is_active": True,
            },
        )

        # 5th organization properties test
        organization = organizations[4]
        assert resource_test(
            organization,
            **{
                "id": 5,
                "parent_id": 4,
                "organization_code": "app-dev",
                "organization_name": "Application Development Dept.",
                "supervisor_id": "0101004",
                "is_active": True,
            },
        )

    def test_get_organization(self):
        organization = self.org_chart.organization.get(4)
        assert isinstance(organization, Organization)
        assert resource_test(
            organization,
            **{
                "id": 4,
                "parent_id": 1,
                "organization_code": "product-dev",
                "organization_name": "Product Development Div.",
                "supervisor_id": "0101003",
                "is_active": True,
            },
        )

    def test_organization_not_found(self):
        with raises(exceptions.ResourceNotFound):
            self.org_chart.organization.get(-1)

    def test_organization_repr(self):
        org = self.org_chart.organization.get(4)
        assert repr(org) == "<{}(id={}): {}>".format(
            Organization.__name__, org.id, org.organization_name
        )

    def test_organization_str_method(self):
        org = self.org_chart.organization.get(4)
        assert str(org) == "<{}(id={}): {}>".format(
            Organization.__name__, org.id, org.organization_name
        )

    def test_get_organization_with_composite_argument(self):
        organization = self.org_chart.organization.get(
            4, organization_code="product-dev"
        )
        assert isinstance(organization, Organization)
        assert resource_test(
            organization,
            **{
                "id": 4,
                "parent_id": 1,
                "organization_code": "product-dev",
                "organization_name": "Product Development Div.",
                "supervisor_id": "0101003",
                "is_active": True,
            },
        )

    def test_too_many_organization_found(self):
        with raises(exceptions.TooManyResource):
            self.org_chart.organization.get(is_active=True)

    def test_get_organization_with_no_filter(self):
        with raises(TypeError):
            self.org_chart.organization.get()


class EmployeeTest(unittest.TestCase):
    def setUp(self):
        self.org_chart = OrgChart("organizations.json", "employees.json")

    def test_list_employees(self):
        employees = self.org_chart.employee.list()
        assert isinstance(employees, list)
        assert len(employees) == 9

        # 1st employee properties test
        employee = employees[0]
        assert resource_test(
            employee,
            **{
                "id": 1,
                "employee_no": "0101001",
                "employee_name": "Bob",
                "email": "bob@mail",
                "onboarding_date": "20200101",
                "organization_id": "president-office",
                "job_title": "President",
                "gender": "M",
            },
        )

        # 2nd employee properties test
        employee = employees[1]
        assert resource_test(
            employee,
            **{
                "id": 2,
                "employee_no": "0101002",
                "employee_name": "Baz",
                "email": "baz@mail",
                "onboarding_date": "20200101",
                "organization_id": "president-office",
                "job_title": "Manager",
                "gender": "F",
            },
        )

        # 3rd employee properties test
        employee = employees[2]
        assert resource_test(
            employee,
            **{
                "id": 3,
                "employee_no": "0101003",
                "employee_name": "Foo",
                "email": "foo@mail",
                "onboarding_date": "20200101",
                "organization_id": "president-office",
                "job_title": "Vice President",
                "gender": "M",
            },
        )

        # 4th employee properties test
        employee = employees[3]
        assert resource_test(
            employee,
            **{
                "id": 4,
                "employee_no": "0101004",
                "employee_name": "Qux",
                "email": "qux@mail",
                "onboarding_date": "20200101",
                "organization_id": "product-dev",
                "job_title": "Manager",
                "gender": "F",
            },
        )

        # 5th employee properties test
        employee = employees[4]
        assert resource_test(
            employee,
            **{
                "id": 5,
                "employee_no": "0101005",
                "employee_name": "Grault",
                "email": "grault@mail",
                "onboarding_date": "20200101",
                "organization_id": "product-design",
                "job_title": "Product Manager",
                "gender": "M",
            },
        )

        # 6th employee properties test
        employee = employees[5]
        assert resource_test(
            employee,
            **{
                "id": 6,
                "employee_no": "0101006",
                "employee_name": "Thud",
                "email": "thud@mail",
                "onboarding_date": "20200201",
                "organization_id": "product-design",
                "job_title": "Designer",
                "gender": "M",
            },
        )

        # 7th employee properties test
        employee = employees[6]
        assert resource_test(
            employee,
            **{
                "id": 7,
                "employee_no": "0101007",
                "employee_name": "Corge",
                "email": "corge@mail",
                "onboarding_date": "20200201",
                "organization_id": "app-dev",
                "job_title": "Engineer",
                "gender": "M",
            },
        )

        # 8th employee properties test
        employee = employees[7]
        assert resource_test(
            employee,
            **{
                "id": 8,
                "employee_no": "0101008",
                "employee_name": "Fred",
                "email": "fred@mail",
                "onboarding_date": "20200201",
                "organization_id": "app-dev",
                "job_title": "Engineer",
                "gender": "M",
            },
        )

        # 9th employee properties test
        employee = employees[8]
        assert resource_test(
            employee,
            **{
                "id": 9,
                "employee_no": "0101009",
                "employee_name": "Amy",
                "email": "amy9@mail",
                "onboarding_date": "20200301",
                "organization_id": "product-design",
                "job_title": "Product Manager",
                "gender": "F",
            },
        )

    def test_get_employee(self):
        employee = self.org_chart.employee.get("thud@mail")
        assert isinstance(employee, Employee)
        assert resource_test(
            employee,
            **{
                "id": 6,
                "employee_no": "0101006",
                "employee_name": "Thud",
                "email": "thud@mail",
                "onboarding_date": "20200201",
                "organization_id": "product-design",
                "job_title": "Designer",
                "gender": "M",
            },
        )

    def test_employee_not_found(self):
        with raises(exceptions.ResourceNotFound):
            self.org_chart.employee.get("corge1@mail")

    def test_employee_repr(self):
        employee = self.org_chart.employee.get("fred@mail")
        assert repr(employee) == "<{}: {}>".format(
            Employee.__name__, employee.employee_name
        )

    def test_employee_str_method(self):
        employee = self.org_chart.employee.get("fred@mail")
        assert str(employee) == "<{}: {}>".format(
            Employee.__name__, employee.employee_name
        )

    def test_employee_cache_in_get_method(self):
        # clear all cache
        self.org_chart.employee.employees_cache = {}

        self.org_chart.employee.get("amy9@mail")

    def test_employee_cache_in_list_method(self):
        # clear all cache
        self.org_chart.employee.employees_cache = {}

        self.org_chart.employee.list()

    def test_get_employee_with_composite_argument(self):
        employee = self.org_chart.employee.get("thud@mail", id=6)
        assert isinstance(employee, Employee)
        assert resource_test(
            employee,
            **{
                "id": 6,
                "employee_no": "0101006",
                "employee_name": "Thud",
                "email": "thud@mail",
                "onboarding_date": "20200201",
                "organization_id": "product-design",
                "job_title": "Designer",
                "gender": "M",
            },
        )

    def test_too_many_employee_found(self):
        with raises(exceptions.TooManyResource):
            self.org_chart.employee.get(organization_id="app-dev")

    def test_get_employee_with_no_filter(self):
        with raises(TypeError):
            self.org_chart.employee.get()


class BasicResourceTest(unittest.TestCase):
    def test_basic_resource_manager(self):
        resource_manager = ResourceManager()
        with raises(NotImplementedError):
            resource_manager.list()
        with raises(NotImplementedError):
            resource_manager.get(1)


class IntegrationTest(unittest.TestCase):
    def test(self):
        orgchart = OrgChart("organizations.json", "employees.json")

        app_dev = orgchart.organization.get(5)
        app_dev_supervisor: Employee = app_dev.supervisor
        assert app_dev_supervisor.email == "qux@mail"

        app_dev_employees = app_dev.employees
        name_list_of_app_dev_employees = [
            employee.employee_name for employee in app_dev_employees
        ]
        assert set(name_list_of_app_dev_employees) == set(["Corge", "Fred"])

        employee_thud = orgchart.employee.get("thud@mail")
        supervisor_of_thud: Employee = employee_thud.supervisor
        assert supervisor_of_thud.employee_name == "Foo"

        organizations_involved_by_thud = employee_thud.organizations
        id_list_of_organizations_involved_by_thud = [
            org.id for org in organizations_involved_by_thud
        ]
        assert id_list_of_organizations_involved_by_thud == [3]

        organizations_involved_by_foo = supervisor_of_thud.organizations
        id_list_of_organizations_involved_by_foo = [
            org.id for org in organizations_involved_by_foo
        ]
        assert set(id_list_of_organizations_involved_by_foo) == set([1, 3, 4])
