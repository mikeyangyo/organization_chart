from typing import List


class Resource:
    pass


class ResourceManager:
    def list(self) -> List[Resource]:
        raise NotImplementedError

    def get(self, id: int) -> Resource:
        raise NotImplementedError
