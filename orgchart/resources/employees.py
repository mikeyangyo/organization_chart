from __future__ import annotations
from typing import Dict, List, Optional, TYPE_CHECKING

from .base import Resource, ResourceManager
from .exceptions import ResourceNotFound, TooManyResource

if TYPE_CHECKING:
    from .organizations import Organization  # pragma: no cover


class Employee(Resource):
    organizations: List[Organization]
    work_organization: Organization

    def __init__(
        self,
        id: int,
        employee_no: str,
        employee_name: str,
        email: str,
        onboarding_date: str,
        organization_id: str,
        job_title: str,
        gender: str,
    ):
        self.id = id
        self.employee_no = employee_no
        self.employee_name = employee_name
        self.email = email
        self.onboarding_date = onboarding_date
        self.organization_id = organization_id
        self.job_title = job_title
        self.gender = gender

    def __str__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.employee_name)

    def __repr__(self):
        return str(self)

    @property
    def supervisor(self) -> Employee:
        return self.work_organization.supervisor


class EmployeeManager(ResourceManager):
    # key is employee id
    employees: Dict[int, Employee] = {}
    # cache employee object with <property key>_<property value>
    employees_cache: Dict[str, Employee] = {}

    def __init__(self, employees: Dict[int, List[Employee]]):
        self.employees = employees

    def list(self, **kwargs) -> List[Employee]:
        result: List[Employee] = []
        for _id in sorted(self.employees.keys()):
            employee_obj: Employee = self.employees[_id]
            for search_key, search_value in kwargs.items():
                # add to cache
                if search_key != "id":
                    cache_key = "{}_{}".format(search_key, search_value)
                    if cache_key not in self.employees_cache.keys():
                        self.employees_cache[cache_key] = employee_obj
                if getattr(employee_obj, search_key) != search_value:
                    break
            else:
                result.append(employee_obj)
        return result

    def get(self, email: Optional[str] = None, **kwargs) -> Employee:
        search_values = kwargs.copy()
        if email is not None:
            search_values["email"] = email
        if not search_values.keys():
            raise TypeError(
                "get() takes at least 1 positional argument (0 given)",
            )
        employees = self.list(**search_values)
        if len(employees) > 1:
            raise TooManyResource("More than one employees to be found")
        elif not employees:
            raise ResourceNotFound("employee not found")
        return employees[0]
