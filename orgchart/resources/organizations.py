from __future__ import annotations
from typing import List, Dict, Optional, TYPE_CHECKING

from .base import Resource, ResourceManager
from .exceptions import ResourceNotFound, TooManyResource

if TYPE_CHECKING:
    from .employees import Employee  # pragma: no cover


class Organization(Resource):
    supervisor: Employee
    employees: List[Employee]

    def __init__(
        self,
        id: int,
        parent_id: int,
        organization_code: str,
        organization_name: str,
        supervisor_id: str,
        is_active: bool,
    ):
        self.id = id
        self.parent_id = parent_id
        self.organization_code = organization_code
        self.organization_name = organization_name
        self.supervisor_id = supervisor_id
        self.is_active = is_active

    def __str__(self):
        return "<{}(id={}): {}>".format(
            self.__class__.__name__, self.id, self.organization_name
        )

    def __repr__(self):
        return str(self)


class OrganizationManager(ResourceManager):
    # key is parent id, to store like tree structure
    organizations: Dict[int, List[Organization]] = {}

    def __init__(self, organizations: Dict[int, List[Organization]]):
        self.organizations = organizations

    def list(self, is_active=True, **kwargs) -> List[Organization]:
        # use BFS algorithm to traverse the organizations
        result: List[Organization] = []
        need_to_visit: List[Organization] = []
        root_org = self.organizations[0][0]
        need_to_visit.append(root_org)

        kwargs["is_active"] = is_active

        while need_to_visit:
            cur_org = need_to_visit[0]
            for search_key, search_value in kwargs.items():
                if getattr(cur_org, search_key) != search_value:
                    break
            else:
                result.append(cur_org)

            need_to_visit = need_to_visit[1:]
            for org in self.organizations.get(cur_org.id, []):
                need_to_visit.append(org)
        return result

    def get(self, id: Optional[int] = None, **kwargs) -> Organization:
        if id:
            kwargs["id"] = id
        if not kwargs:
            raise TypeError(
                "get() takes at least 1 positional argument (0 given)"
            )

        result = self.list(**kwargs)

        if len(result) > 1:
            raise TooManyResource("More than one organizations to be found")
        if not len(result):
            raise ResourceNotFound("organization not found")
        return result[0]
