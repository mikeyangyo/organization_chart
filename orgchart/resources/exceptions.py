class ResourceError(Exception):
    pass


class ResourceNotFound(ResourceError):
    # means that no resource was found
    pass


class TooManyResource(ResourceError):
    # means that too many resource were found
    pass
