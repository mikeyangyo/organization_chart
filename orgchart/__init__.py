from json import load
from .resources import (
    Organization,
    OrganizationManager,
    Employee,
    EmployeeManager,
)


class OrgChart:
    organization: OrganizationManager
    employee: EmployeeManager

    def __init__(self, org_file_path: str, employee_file_path: str):
        org_file_content = ""
        employee_file_content = ""
        organizations = {}
        employees = {}

        # fetch organizations
        with open(org_file_path, "r") as f:
            org_file_content = load(f)
        for org in org_file_content:
            if not organizations.get(org["parent_id"]):
                organizations[org["parent_id"]] = []
            org_obj = Organization(**org)
            organizations[org["parent_id"]].append(org_obj)
        self.organization = OrganizationManager(organizations)

        # fetch employees
        with open(employee_file_path, "r") as f:
            employee_file_content = load(f)
        for employee in employee_file_content:
            employee_obj = Employee(**employee)

            # find org which this employee worked in
            work_org_obj = self.organization.get(
                organization_code=employee_obj.organization_id
            )
            employee_obj.work_organization = work_org_obj
            if not hasattr(work_org_obj, "employees"):
                work_org_obj.employees = []
            work_org_obj.employees.append(employee_obj)

            # find org supervised by this employee
            supervised_org_objects = self.organization.list(
                supervisor_id=employee_obj.employee_no,
            )
            for org in supervised_org_objects:
                org.supervisor = employee_obj

            employee_obj.organizations = [
                work_org_obj
            ] + supervised_org_objects
            employees[employee_obj.id] = employee_obj
        self.employee = EmployeeManager(employees)
