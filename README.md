# Organization-Chart

[![codecov](https://codecov.io/gl/mikeyangyo/organization_chart/branch/main/graph/badge.svg)](https://codecov.io/gl/mikeyangyo/organization_chart)



orgchart is a `tool` that allows companies which have no HRMS to do query human resource data.

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the `Python 3.8`

## Installing orgchart

To install orgchart, follow these steps:

Linux and macOS:
```
python3 -m pip install --index-url https://test.pypi.org/simple/ orgchart
```

Windows:
```
py -m pip install --index-url https://test.pypi.org/simple/ orgchart
```
## Using orgchart

To use orgchart, follow these steps:

```python3
from orgchart import OrgChart

org_chart = OrgChart("organizations.json", "employees.json")

# to list organizations
orgs = org_chart.organization.list()
print(orgs)

# to get organization which id is 5
app_dev = org_chart.organization.get(5)
print(app_dev)

# to get supervisor of organization app_dev
print(app_dev.supervisor)

# to get employees in organization app_dev
print(app_dev.employees)

# to get certain employee
thud = org_chart.employee.get("thud@mail")
print(thud)

# to get employee's supervisor
print(thud.supervisor)

# to get organization which employee involved in
print(thud.organizations)
```

## Contributing to orgchart
To contribute to orgchart, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Run the test cases: `pytest tests`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contact

If you want to contact me you can reach me at <perryvm06vm06@gmail.com>.

## License
This project uses the following license: [MIT](/LICENSE).
